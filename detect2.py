# USAGE: python detect2.py -i ipAddress, -u Camera User Name, -p Password
# Picture can be divied into zones to increase resolution, or activate different zones over time
# This program is based on Adrian Rosebrocks "Pedestrian detection OpenCV", http://www.pyimagesearch.com/2015/11/09/pedestrian-detection-opencv/

# Todo: remove the image store ondisk (output.jpg)

from __future__ import print_function
from imutils.object_detection import non_max_suppression
import numpy as np
from requests.auth import HTTPDigestAuth
import imutils
import cv2
import requests
import shutil
from datetime import datetime
import argparse


# Debug timer
# returns the elapsed milliseconds since the start of application
def millis():
    dt = datetime.now() - start_time
    ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
    return int(ms)

# Check if the same rectangle is repeated over two or more pictures
# Return Rectangle data
#This function may be omitted!
def checkHistory(zoneNumber):
    global pick, zoneHist
    lPick = len(pick)
    if lPick == 0: # No rects in zone: Clear history
        #print ("ZoneNumber", zoneNumber, zoneHist)
        zoneHist[zoneNumber] = []
        return
    for r in range (lPick): # For each found rectangle
        print ("Rectangle ",pick[r])
        newRect = True
        for m in zoneHist[zoneNumber]: # Check all rects in zone
            if (abs(m[0]-pick[r][0]) < 10) and (abs(m[1]-pick[r][1]) < 10): # x and y coordinates close to each
                print ("Rect already found", r, m)
                newRect = False
                break
        if newRect:
            zoneHist[zoneNumber].append(pick[r])
            print ("This is a new Rect! Hist", zoneHist)
    return newRect

# This module analysis the image and make a rectangle for each pedistrian
# image: Image to analyze
# z: 
def analysisImage(image1, z):
    global dPictureCounter, zoneHist, pick, zones

    image=image1[zones[z][0]:zones[z][1], zones[z][2]:zones[z][3]] # Cut out actual zone
    image = imutils.resize(image, width=min(400, image.shape[1]))
    orig = image.copy()
    # detect people in the image
    (rects, weights) = hog.detectMultiScale(image, winStride=(4, 4),
        padding=(16, 16), scale=1.05, hitThreshold =0.3)

        
    # draw the original bounding boxes
    for (x, y, w, h) in rects:
        cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)

    # apply non-maxima suppression to the bounding boxes using a
    # fairly large overlap threshold to try to maintain overlapping
    # boxes that are still people
    rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
    pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

    # draw the final bounding boxes
    for (xA, yA, xB, yB) in pick:
        cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
    #cv2.imwrite('debug.jpg', image)
    
    for r in pick: # For each found rectangle
        dPictureCounter=dPictureCounter+1
        cv2.imwrite('result1//_'+str(dPictureCounter)+'_'+str(weights)+'.jpg',image)

    # show some information on the number of bounding boxes
    #print("[INFO] {} original boxes, {} after suppression".format(len(rects), len(pick)))

    return len(rects)

#
# Initialize data
#

zones=[[0,480, 960,1600], [100,1080, 0,1600]] #Home (yStart:yStop, xStart:xStop)

#zoneHist is used to verify if rectangles are repeated in pictrures 
zoneHist = [] # x,y point of old rectangles
for n in zones:
    zoneHist.append([]) # Add one list for each zone. Each zone list shall contain list of found rectangles

dPictureCounter=0
start_time = datetime.now()

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-u", "--user", required=True, help="user name")
ap.add_argument("-p", "--pass", required=True, help="password")
ap.add_argument("-i", "--ip", required=True, help="Camera IP address")
args = vars(ap.parse_args())

user = args["user"]
pw = args["pass"]
ip = args["ip"]

# initialize the HOG descriptor/person detector
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())


while True:
    t1=millis()    # Debug: Performance timer
    #Read image from Cam
    for i in range(3, 0, -1):
        try:
            response = requests.get('http://'+ip+'/axis-cgi/jpg/image.cgi?resolution=1280x960&compression=25&camera=1&color=1', auth=HTTPDigestAuth(user,pw), timeout=10, stream=True)
        except IOError:
            if i==0:
                raise
            print (" Retry HTTP get")
        else:
            break
    if response.status_code == 200:
        with open('output.jpg', 'wb') as fd:
            response.raw.decode_content = True
            shutil.copyfileobj(response.raw, fd) 
        fd.close()

    else:
        print ("Cam error: "+str(response.status_code) )
        break
    
    # load the image and resize it to (1) reduce detection time
    # and (2) improve detection accuracy
    
    image = cv2.imread('output.jpg')
    
    t2=millis() # Debug: Performance timer
    # Check all zones in image
    count = 0
    for z in range (len(zones)):
        count = count + analysisImage(image,z)
        checkHistory(z)
    t3=millis()  # Debug: Performance timer
    #if count:
    #print("[INFO] Cam [mS] {}, Eval * X [mS] {}, Rects in zone {}".format(t3-t2, t2-t1, count))
            
